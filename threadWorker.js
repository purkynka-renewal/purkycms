//
// Libs
//
const { worker } = require("cluster");
const debug = require("debug")("worker");
const fs = require("fs");
const YAML = require("yaml");
const zhIPC = require("./ZhIPC");
const Purky = require("./purky");
const lang = require("./lang.js");
const HTTPServer = require("./HTTPServer");

if (global.isAdmin){
  debug.namespace += ":"+"adminWorker";
} else {
  debug.namespace += ":"+"serviceWorker"+":"+worker.id;
}

//
// Load types
//
const types = {};
const typesLang = {};
const typesPath = CONFIG.paths.config._base+CONFIG.paths.config.types;
fs.readdirSync(typesPath).forEach((item) => {
  const filename = typesPath+"/"+item;
  if (
    !(item.endsWith(".yml") || item.endsWith(".yaml")) ||
    item.startsWith("_") || filename == __filename
  ) return;

  const file = fs.readFileSync(filename).toString();
  const type = YAML.parse(file);
  const typename = item.replace(/\.(?:yml|yaml)$/, "");
  types[typename] = type;

  // Do some defaults
  if (global.isProduction) {
    type.baseurl = ("/"+(type.baseurl || typename)+"/").replace(/\/+/g,/*/ */ "/");
  } else {
    type.baseurl = "/"+typename+"/";
  }


  // Apply types-related language
  for (const [langName, value] of Object.entries(type.lang || {})){
    if (!(langName in typesLang)) typesLang[langName] = {};
    Object.assign(typesLang[langName], value);
  }
});

lang.onUpdate = function () {
  for (const [langName, value] of Object.entries(typesLang)){
    if (!(langName in this.langs)) this.langs[langName] = {};
    Object.assign(this.langs[langName], value);
  }
}
lang.onUpdate();

//
// START
//
reloadConfig();
const http = new HTTPServer(lang, global.isSlave);
const purky = new Purky({
  dbpath  : CONFIG.paths.data._base+CONFIG.paths.data.db,
  readOnly: !isAdmin
});
purky.load(types);
http.start();
http.purky = purky;

if (!isAdmin) {
  // Report requests to Master
  http.onRequest = function(){
    zhIPC.request("recievedHTTPRequest", worker.id);
  };
}


//
// Thread comms
//
zhIPC
  // Exit gracefully
  .on("shutdown", (resolve)=>{
    resolve();
    // If we don't exit until connection Timeout, forceExit
    setTimeout(forceExit, CONFIG.conTimeout).unref();
  })
  // Reload configuration
  .on("reloadConfig", (done)=>{
    reloadConfig();
    http.updateConfig();
    lang.update();
    done();
  });

//
// Functions
//
function reloadConfig(){
  Object.assign(global.CONFIG, YAML.parse(fs.readFileSync(
    CONFIG.paths.config._base+CONFIG.paths.config.conf
  ).toString()));
}

function forceExit(){
  purky.close();
  http.server.unref();
  http.stop();
}

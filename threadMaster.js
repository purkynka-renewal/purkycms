//
// Libs
//
const debug = require("debug")("master");
const cluster = require("cluster");
const fs = require("fs");
const YAML = require("yaml");
const ms = require("ms");
const { mkdir } = fs.promises;
const zhIPC = require("./ZhIPC");

//
// Init
//
debug.enabled = true;
const nCpus = require('os').cpus().length;
const CONFIG = global.CONFIG;
let adminWorker, threadFails = 0, minThreads = 1, maxThreads = 1;
const status = {
  workers: {},
};

//
// Start
//
loadConfig();
start();

//
// Functions
//
function loadConfig() {
  Object.assign(CONFIG, YAML.parse(fs.readFileSync(
    CONFIG.paths.config._base+CONFIG.paths.config.conf
  ).toString()));

  if (CONFIG.threads.min == "auto") minThreads = 1;
  else minThreads = Math.min(nCpus, Math.max(1, CONFIG.threads.min));
  if (CONFIG.threads.max == "auto") maxThreads = nCpus;
  else maxThreads = Math.min(nCpus, Math.max(1, CONFIG.threads.max));

  const _minThreads = minThreads, _maxThreads = maxThreads;
  minThreads = Math.min(_minThreads, _maxThreads);
  maxThreads = Math.max(_minThreads, _maxThreads);
}

async function start() {
  debug("Initializing...");

  //
  // INIT
  //
  // Make sure all needed folders exist
  const folderPromises = [];
  const data_base = CONFIG.paths.data._base;
  if (!fs.existsSync(data_base)) folderPromises.push(mkdir(data_base));
  for (const [key, path] of Object.entries(CONFIG.paths.data)) {
    if (!key.startsWith("_") && !fs.existsSync(data_base+path)) {
      folderPromises.push(mkdir(data_base+path));
    }
  }
  await Promise.allSettled(folderPromises);
  // Train PurkyAI
  await require('./purkyAIManager').train(false);

  debug("Starting workers...");
  cluster.setupMaster({
    silent: isProduction
  });

  //
  // START
  //
  // spawn admin
  spawnThread(true);

  // Wait for adminWorker to start and start a service worker
  adminWorker.on('listening', ()=>{
    for(let i = minThreads; i-- != 0;){
      spawnThread();
    }

    debug("Ready");
  });


  //
  // Thread comms
  //

  zhIPC
    // Monitor worker activity
    .on("recievedHTTPRequest", (workerID, resolve)=>{
      resolve(); // No need to wait, just confirm delivery
      const workerStat = status.workers[workerID];
      // Keep the worker alive
      workerStat.touch();
      // Check for overload if there is a place for new thread
      if (getServiceThreadsN() < maxThreads) {
        if (Object.values(status.workers)
            .every(w=>w.nRequests >= CONFIG.threads.overloadCount)){
          // Spawn a new service worker
          spawnThread();
        }
      }
    })
    // Send info about threads
    .on("getThreadInfo", (response)=>{
      response({
        threadFails, maxThreads, minThreads, threads: getServiceThreadsN()
      });
    })
    // Command to reload configuration
    .on("reloadConfig", (done)=>{
      reloadConfig().then(done);
    });
}


function spawnThread(isAdmin=false){
  const worker = (cluster.fork(isAdmin ? {PURKY_MODE: "admin"} : {})
    // Restart worker on error
    .on('exit', ()=>{
      delete status.workers[worker.id];

      if (worker.exitedAfterDisconnect) return;

      threadFails++;

      if (isAdmin || Object.values(cluster.workers).length < minThreads) {
        spawnThread(isAdmin);
      }
    })
  );

  if(isAdmin) adminWorker = worker;
  else {
    const workerStat = status.workers[worker.id] = {
      worker,
      nRequests: 0,
      exitting: false,
      deathTimeout: null,
      touch(){
        this.nRequests++;
        if (this.deathTimeout) clearTimeout(this.deathTimeout);
        this.deathTimeout = setTimeout(()=>{
          // Keep it alive if it's last
          if (getServiceThreadsN() == 1) {
            // Reset request counter
            this.nRequests = 0;
            return this.touch();
          }
          // Gracefully disconnect otherwise
          this.exitting = true;
          zhIPC.send("shutdown", this.worker);
          this.worker.disconnect();
        }, ms(CONFIG.threads.idleTimeout));
      }
    };
    workerStat.touch(); // Start autodie
  }
  return worker;
}


async function reloadConfig(){
  loadConfig();
  return zhIPC.broadcast("reloadConfig");
}

function getServiceThreadsN(){
  let serviceThreadsN = 0;
  for (const worker of Object.values(cluster.workers)) {
    if (worker == adminWorker) continue;
    if (!status.workers[worker.id].exitting) serviceThreadsN++;
  }
  return serviceThreadsN;
}

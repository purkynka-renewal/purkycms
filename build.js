#!/usr/bin/env node

const { spawn } = require('child_process');
const promises = [];

function buildSubmodule(path){
  promises.push(new Promise((resolve)=>{
    console.log("Building "+path);
    const proc = spawn("npm", ["run", "build"], {
      cwd:__dirname+"/"+path,
      stdio: "inherit"
    });
    proc.on('close', resolve);
  }));
}

buildSubmodule("frontend-admin");
buildSubmodule("frontend");

module.exports = promises;

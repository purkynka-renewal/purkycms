const debug   = require('debug')("purkyAI");
const PurkyAI = require('../purkyai');
const train   = require('../purkyai/train.js');
// const { Worker }  = require('worker_threads');

module.exports = async function spawnPurkyAI(context={}) {
  debug("Starting...");
  const purkyAI = new PurkyAI(context);
  await purkyAI.load();

  // function spawnDiscordBot() {
  //   const discordbot = new Worker(__dirname+"/_discordbot.js");
  //   discordbot.on('message', async ({channelID, message})=>{
  //     const result = await purkyAI.process(message);
  //     discordbot.postMessage({
  //       channelID, message: result.answer
  //     });
  //   });
  //   discordbot.on('exit', (code)=>{
  //     if(code != 0) setTimeout(spawnDiscordBot, 1000); // respawn
  //   });
  // }
  // spawnDiscordBot(); // Disabled

  debug("Ready");
  return purkyAI;
}

module.exports.train = train;

const CONFIG = global.CONFIG;
const fs        = require("fs");
const cors      = require("cors");
const YAML      = require("yaml");
const debug     = require("debug")("http");
const Server    = require("http").createServer;
const helmet    = require('helmet');
const express   = require('express');
const socketio  = require('socket.io');
const nunjucks  = require('nunjucks');
const favicon   = require('serve-favicon')
const bodyParser    = require('body-parser');
const earlyHints    = require('early-hints')
const cookieParser  = require('cookie-parser');
const Locale = require("./locale");
const purkyAIMan    = require('../purkyAIManager');
const router = global.isAdmin ? require("./adminRouter") : require("./frontRouter");
const mainMiddleware = require("./mainMiddleware");
const FileSystem = require("../filesystem/socketRouter");
const FRONTEND_CONFIG = require("../"+CONFIG.paths.root+"config.json");

module.exports = class HTTPManager{
  onRequest(){}

  constructor(lang){
    debug("Initializing...");
    this.lang   = lang;
    this.ready  = this.runnning = false;
    this.purky  = null;
    this.host   = CONFIG.host;
    if(global.isAdmin){
      this.port       = CONFIG.aport;
      this.canonical  = CONFIG.urls.admin;
    }else{
      this.port       = CONFIG.sport;
      this.canonical  = "/";
    }
    if ("serverPush" in FRONTEND_CONFIG) {
      FRONTEND_CONFIG.serverPush = FRONTEND_CONFIG.serverPush.map(item=>({
        ...item, path: this.getLink(CONFIG.urls.static+"/"+item.path)
      }));
    }
    debug("Starting PurkyAI...");
    this.purkyAI = {
      ready: false,
      process: msg=>this.purkyAIPrm.then(i=>i.process(msg))
    };
    this.purkyAIPrm = purkyAIMan().then((i)=>this.purkyAI=i);

    /*
     *
     * Define the web server
     *
     */
    // Express
    const http = this;
    const app     = this.app    = express();
    const server  = this.server = Server(app);
    // Socket.io
    const io      = this.io     = socketio(server, {
      path: this.getLink("/socket"),
      // serveClient: false,
      transports: ['websocket', 'polling'],
    });
    // FileSystem
    this.filesystem = new FileSystem(
      CONFIG.paths.data._base+CONFIG.paths.data.files,
      CONFIG.paths.data._base+CONFIG.paths.data.thumbs
    );

    // Engines
    const njkEnv = this.njkEnv  = nunjucks.configure(
      CONFIG.paths.root + CONFIG.paths.views, {
      autoescape: true,
      express   : app,
      watch     : !global.isProduction,
      // dev       : !global.isProduction // what's this?
    });
    this.extendNunjucks(njkEnv);

    // Settings
    app.set("trust proxy", true);

    // Middlewares
    app.use(cors({origin:true}));
    app.use(helmet());
    app.use(favicon(CONFIG.paths.root+CONFIG.paths.favicon));
    app.use(this.getLink("/serviceWorker.js"), (req, res) => {
      req.url = this.getLink(
          CONFIG.urls.static+"/"+FRONTEND_CONFIG.serviceWorker.path);
      return app.handle(req, res);
    });
    // static
    app.use(this.getLink(CONFIG.urls.static),
        express.static(CONFIG.paths.root+CONFIG.paths.static, {
          maxAge: global.isProduction ? "1d" : 1
    }));
    // files
    app.use(this.getLink(CONFIG.urls.files),
        express.static(CONFIG.paths.data._base+CONFIG.paths.data.files));
    // thumbnails
    app.use(this.getLink(CONFIG.urls.thumbs),
        express.static(CONFIG.paths.data._base+CONFIG.paths.data.thumbs));
    // Server push
    if (FRONTEND_CONFIG.serverPush.length){
      // TODO: Add nopush to prevent NGINX pushing if the client has visited the site before
      //app.use(earlyHints(FRONTEND_CONFIG.serverPush));
    }
    app.use(cookieParser());
    app.use((this.locale = Locale()).middleware);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));

    if(global.isAdmin){
      // Login interface
      app.post(this.getLink("/login"), (req,res)=>{
        this.purky.resolveURI(
          "/accounts/login", req.query, req.body, "POST"
        ).then((result)=>{
          if(result.code == 200){
            let target = req.get('Referrer');
            if (isProduction && this.purky.acc_placeholder) {
              target =
                  this.absolutizeURL(this.getLink("/create-first-acc"), req);
            }
            const url = new URL(target);
            url.searchParams.append("fullBody", true);
            return res.cookie('token', result.data.result.token)
                .redirect(url.href);
          }
          res.status(401).redirect("back");
        });
      });
    }

    //
    // Socket.io middlewares
    //
    // Main middleware
    this.ioMiddleware = function(){
      return (socket, next)=>{
        const req = socket.request, res = socket.request.res;
        cookieParser()(req, res, ()=>{
          http.locale.middleware(req, res, next);
        });
      };
    };
    // Security middleware
    this.ioSecurityMiddleware = (socket, next)=>{
      if (this.purky.checkToken(socket.request.cookies.token)) return next();
      else return socket.disconnect(true);
    };

    //
    // App
    //
    app.use(mainMiddleware(this));
    app.use(this.getLink("/"), router(this));
    app.use(this.getLink(CONFIG.urls.api._base),
        (req,res,next)=>this.purky.router(req, res, next));
    app.get(this.getLink("/offline"), (_, res) => res.ctxRender("offline"));

    //
    // Error handler
    //
    app.use(function fatalErrorHandler(err, _, res, next){
      if (err && !("ctxRender" in res)) { // Error happened before Main middleware
        return res.status(err.status || err.statusCode || 500)
            .send(global.isProduction ? err.message : err.stack);
      }
      next(err);
    });
    app.use(function errorHandler (err, _, res, next){
      if (!err) return next();
      else if (err.name == "NoToken") return res.status(401).ctxRender("login");
      else if (err.name == "NotReady") {
        return (res
          .status(503)
          .set({"Retry-After": CONFIG.reqPerSec})
          .ctxRender("notReady")
        );
      }
      else return res.status(err.status || 500).ctxRender("error", {
        name: err.name,
        status: err.status || 500,
        message: err.message || ""
      });
    });
    // redirect to base url if needed
    if (this.getLink("/") != "/") {
      // app.use((req, res, next)=>{
      //   if (req.url.length >= this.getLink("/").length) return next();
      //   res.redirect(this.getLink("/"));
      // });
    }
    // 404
    app.use(function NotFoundHandler(_, res){
      return res.status(404).ctxRender("error", {status:404});
    });

    debug("Ready");
    this.ready = true;

    this.updateConfig();
    if(global.isProduction) this.precompile();
  }

  /**
   * updateConfig - description
   *
   * @return {type}  description
   */
  updateConfig(){
    if(!this.ready) return;
    debug("Updating configuration...");

    this.locale.update(
      Object.keys(this.lang.langs),
      CONFIG.defLocale || "default"
    );

    const envPath = CONFIG.paths.config._base+CONFIG.paths.config.env;
    const cfgPath = CONFIG.paths.config._base+CONFIG.paths.config.theme;
    const menuPath= CONFIG.paths.config._base+CONFIG.paths.config.menu;

    // Load vars
    const env = YAML.parse(fs.readFileSync(envPath, "utf-8").toString());
    const cfg = YAML.parse(fs.readFileSync(cfgPath, "utf-8").toString());

    if(this.purkyAIPrm){
      debug("Updating PurkyAI context...");
      if(this.purkyAI.ready){
        this.purkyAI.updateContext(env);
      }else{
        this.purkyAIPrm.then(()=>{
          this.purkyAI.updateContext(env);
        });
      }
    }

    const globEnv = Object.assign({}, CONFIG, env, cfg);
    for(const [key, value] of Object.entries(globEnv)){
      this.njkEnv.addGlobal(key, value);
    }


    // Load menus
    const menuDir = fs.readdirSync(menuPath, {withFileTypes: true});
    const menu = {};
    for(const file of menuDir){
      if(!file.isFile() || !file.name.endsWith(".yml")) continue;
      const data = fs.readFileSync(menuPath+file.name, "utf-8").toString();
      menu[file.name.replace(/\.yml$/, "")] = YAML.parse(data);
    }
    this.njkEnv.addGlobal("menu", menu);

    debug("Configuration updated");
  }

  /**
   * precompile - description
   *
   * @return {type}  description
   */
  precompile(){
    debug("(Re)Precompiling templates...");
    const dir = fs.readdirSync(
      CONFIG.paths.root + CONFIG.paths.views,
      {withFileTypes: true}
    );

    for (const file of dir){
      if(!file.isFile() || file.name.startsWith("_")) continue;

      try{
        this.njkEnv.getTemplate(file.name, true);
      }catch(e){debug(e)}
    }
    debug("Precompiling finished");
  }

  /**
   * start - Starts serving HTTP
   */
  start(callback=()=>{}){
    this.server.listen({
      port: this.port,
      host: this.host,
      // path: "run/balancer.sock",
    }, ()=>{
      const {port,address} = this.server.address();
      debug("Listening on "+address+":"+port);
      callback(address, port);
      this.runnning = true;
    });
  }

  /**
   * stop - Stops serving HTTP
   */
  stop(callback=()=>{}){
    this.runnning = false;
    this.server.close(callback);
    this.server = null;
  }
}

Object.assign(module.exports.prototype, require("./extendNunjucks.js"));

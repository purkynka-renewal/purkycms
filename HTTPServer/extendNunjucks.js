module.exports = {
  absolutizeURL(url, req){
    const match = url.match(urlRegExp);
    if(!match) return url;

    const protocol = match.groups.protocol || req.protocol;
    const host = match.groups.host || req.hostname;
    const currentPath = req.originalUrl;
    const uri = (match.groups.uri || "")
        .replace(/^\.\./, currentPath.split("/").slice(0,-2).join("/"))
        .replace(/^\./, currentPath)
        .replace(/^\//, "")
        .replace(/\/+/g, /*/ */ "/");

    // don't recreate non-http urls
    if(!protocol.match(/^https?:?$/)) return url;
    let port = "";
    if (host == "localhost") port = ":"+this.port;

    return protocol+"://"+host+port+"/"+uri;
  },

  chooseLanguageVersion(obj, locale="en"){
    return (
      obj[locale] ||
      obj[global.CONFIG.defLocale] ||
      obj[global.CONFIG.fallbackL] ||
      Object.values(obj)[0]
    );
  },

  /**
   * translate (__) - tries to translate given string or returns the string
   *
   * @param {String} str    the string to translate
   * @param {String} locale locale to preferably translate to
   */
  translate(aStr, locale="en"){
    aStr = decodeURIComponent(String(aStr));
    const str = aStr.toLowerCase();
    return (
      this.lang.langs[locale][str] ||
      this.lang.langs[global.CONFIG.defLocale][str] ||
      this.lang.langs[global.CONFIG.fallbackL][str] ||
      aStr
    );
  },

  reverseTranslate(aStr, locale="en"){
    aStr = decodeURIComponent(String(aStr));
    const str = this.purky.slugify(aStr.toLowerCase());
    let index, lang;
    // find the translation
    (findIndex = ()=> {
      lang  = this.lang.langs[locale];
      index = Object.values(lang).map(this.purky.slugify).indexOf(str);
      if(index != -1) return;
      // default
      lang  = this.lang.langs[global.CONFIG.defLocale];
      index = Object.values(lang).map(this.purky.slugify).indexOf(str);
      if(index != -1) return;
      // fallback
      lang  = this.lang.langs[global.CONFIG.fallbackL];
      index = Object.values(lang).map(this.purky.slugify).indexOf(str);
    })();
    if(index == -1) return aStr;
    return Object.keys(lang)[index];
  },

  /**
   * getLink - generates a link to given URL
   *
   * @param {String} URL The URL to generate link to
   */
  getLink(url, originalUrl="/", locale=false) {
    url = String(url);
    let newUrl;
    // relative
    if (url.startsWith(".")) {
      originalUrl = normalizeUrl(originalUrl.split("#")[0].split("?")[0]+"/");
    }
    // relative with .
    if (url.startsWith("./")) {
      newUrl = originalUrl+url.substr(1);
    }
    // relative with step up
    else if (url.startsWith("../")) {
      originalUrl = originalUrl.split("/").slice(0, -2).join("/");
      newUrl = originalUrl+url.substr(2);
    }
    // absolute
    else if(url.startsWith("/")) {
      newUrl = this.canonical+url;
    // relative or canonical
    } else {
      newUrl = url.replace(/^~/, global.isProduction? "" : this.canonical);
    }
    newUrl = normalizeUrl(newUrl);
    if (locale) newUrl = this.getLocalizedURL(newUrl, locale);
    return newUrl.split("/").map(encodeURIComponent).join("/");
  },

  getLocalizedURL(url, locale="en", reverse=false){
    url = url.split("?");
    const query = url[1];
    url = url[0].split("#");
    const hash = url[1];
    url = url[0];

    const newUrl = [];
    for (const comp of url.split("/")) {
      if (reverse) newUrl.push(this.getUnlocalizedURLComp(comp, locale));
      else newUrl.push(this.getLocalizedURLComp(comp, locale));
    }
    return newUrl.join("/")+(query?"?"+query:"")+(hash?"#"+hash:"");
  },

  getUnlocalizedURL(url, locale="en"){
    return this.getLocalizedURL(url, locale, true);
  },

  getLocalizedURLComp(str, locale="en"){
    return this.purky.slugify(this.translate(str, locale));
  },

  getUnlocalizedURLComp(str, locale="en"){
    return this.purky.slugify(this.reverseTranslate(str, locale));
  },

  getContent(url, query, body){
    return this.purky.resolveURI(url, query, body, "GET");
  },

  /**
   * extendNunjucks - description
   *
   * @return {type}  description
   */
  extendNunjucks(njkEnv) {
    const http = this;
    (njkEnv
      .addGlobal("title_separator", global.CONFIG.title_sep)
      .addGlobal("getSelf", function getSelf() { return this.ctx; })

      /**
       * translate (__) - returns translation of the string or the string itself
       *
       * @param {String} str the string to translate
       */
      .addGlobal("__", function translate(str) {
        return http.translate(str, this.ctx.req.locale);
      })

      /**
       * untranslate (_n) -
       *
       *
       */
      .addGlobal("_n", function untranslate(str) {
        return http.getUnlocalizedURLComp(str, this.ctx.req.locale);
      })

      /**
       * chooseLanguageVersion (chooseLangVer) -
       *
       *
       */
      .addGlobal("chooseLangVer", function chooseLanguageVersion(obj) {
        return http.chooseLanguageVersion(obj, this.ctx.req.locale);
      })

      /**
       * getLink - generates a link to given URL
       *
       * @param {String} URL The URL to generate link to
       */
      .addGlobal("getLink", function (url, localise=true) {
        const locale = localise ? this.ctx.req.locale : false;
        return http.getLink(url, decodeURI(this.ctx.req.originalUrl), locale);
      })

      /**
       * getAsset - generates a URL to given resource
       *
       * @param {String} resource The resource to link
       */
      .addGlobal("getAsset", function(url){
        return http.getLink(CONFIG.urls.static+"/"+url);
      })

      /**
       * getFile - generates a URL to given resource
       *
       * @param {String} resource The resource to link
       */
      .addGlobal("getFile", function(url){
        return http.getLink("~"+CONFIG.urls.files+"/"+url);
      })

      /**
       * getThumbnail - generates a URL to given resource
       *
       */
      .addGlobal("getThumbnail", function(...args){
        return http.getLink(
              CONFIG.urls.thumbs+"/"+http.filesystem.getThumbnail(...args));
      })

      /**
       * randString - generates random string
       *
       * @param {Number} len length of the generated string
       */
      .addGlobal("randString", function(len=8){
        if(http.purky) return http.purky.getRandomString(len);
        return null;
      })


      /**
       * slugify -
       *
       *
       */
      .addFilter("slugify", function(str){
        if(http.purky) return http.purky.slugify(str);
        return str;
      })

      /**
       * getThumbnail
       *
       */
      .addFilter("thumbnail", function(...args){
        return http.getLink(
              CONFIG.urls.thumbs+"/"+http.filesystem.getThumbnail(...args));
      })

      /**
       * localizeURL -
       *
       *
       */
      .addFilter("localizeURL", function(str){
        if(http.purky) return http.getLocalizedURL(str, this.ctx.req.locale);
        return str;
      })

      /**
       * link -
       */
      .addFilter("link", function(url){
        return http.absolutizeURL(url, this.ctx.req);
      })

      /**
       * transslugify -
       *
       *
       */
      .addFilter("transslugify", function(str){
        if(http.purky) return http.getLocalizedURLComp(str, this.ctx.req.locale);
        return str;
      })

      /**
       * isOfType
       */
      .addFilter("isOfType", function(obj, type){
        return typeof obj === type;
      })

      /**
       * number
       */
      .addFilter("number", function(obj){
        return Number(obj);
      })

      /**
       * json
       */
      .addFilter("json", function(obj){
        return JSON.stringify(obj);
      })

      /**
       * slice
       */
      .addFilter("slice", function(arr, start, end){
        return arr.slice(end, start);
      })

      /**
       * split
       */
      .addFilter("split", function(str, delimiter){
        return str.split(delimiter);
      })

      /**
       * length
       */
      .addFilter("length", function(obj){
        return (Array.isArray(obj) ? obj : Object.keys(obj)).length;
      })

      /**
       * num2word
       */
      .addFilter("num2word", function(num){
        return [
          null, "one", "two", "three", "four", "five", "six", "seven", "eight",
          "nine", "ten", "elevn", "twelve", "thirteen", "fourteen", "fifteen",
          "sixteen"
        ][num] || null;
      })
    );
    return;
  }
}

function normalizeUrl(url="") {
  return url.replace(/\/+/g,/*/ */ "/");
}

const urlRegExp = new RegExp("(?<protocol>\\w*)?" +
    "(?:(?::\\/\\/)?(?<host>\\w[\\w\\-\\.:]+\\.\\w+))?" +
    "(?<uri>\\.?\\.?\\/.*)?");

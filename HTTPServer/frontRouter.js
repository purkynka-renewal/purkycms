const {Router}  = require('express');

module.exports = function(http){
  const router = new Router();

  router.get("/", (_,res)=>{
    res.ctxRender("index");
  });

  router.get("/faq", (_,res)=>{
    res.ctxRender("faq");
  });

  http.io.of("/purkyai").on('connect', (sock) => {
    sock.on("message", (msg, res)=>{
      http.purkyAI.process(msg).then(res);
    });
  });

  // router.get("*", async (req,res,next)=>{
  //   const result = await http.purky.resolveURI(req.originalUrl, req.query, req.body);
  //   res.status(result.code).json(result.data);
  // });

  return router;
};

const Locale = require("locale");

module.exports = function (supported=[], defLang="en") {
  const obj = {
    supported: supported,
    defLang: defLang,
    update (aSupported, aDefLang) {
      obj.supported = aSupported || supported;
      obj.defLang = aDefLang || defLang;
      obj.locale = Locale(obj.supported, obj.defLang);
    },
    middleware (req, res, next) {
      const cookie = req.cookies && req.cookies.locale;
      if (cookie && obj.supported.includes(cookie)) {
        req.headers["accept-language"] = cookie+","+
            (req.headers["accept-language"] || obj.defLang);
      }
      return obj.locale(req, res, next);
    },
  };
  obj.update();
  return obj;
}

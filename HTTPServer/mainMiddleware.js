const _E = require("../errors.js");

module.exports = function getMainMiddleware(http) {
  return function mainMiddleware(req,res,next){
    // Report request to Master
    http.onRequest();

    // Set connection timout
    req.connection.setTimeout(global.CONFIG.conTimeout);

    //
    // Register methods
    //

    Object.assign(res, {
      next, getTemplateName,

      /**
       * getContext - description
       *
       * @param  {type} ctx description
       * @return {type}     description
       */
      getContext(ctx={}) {
        return Object.assign({
          CONFIG: global.CONFIG,
          isProduction: global.isProduction,
          isAJAX: req.xhr && !this.req.query.fullReload,
          isFullBody: this.req.query.fullBody,
          isTable: !!this.req.get("X-Requested-Table"),
          req: this.req,
          user: this.req.user,
          contentTypes: http.purky.types,
          locales: Object.keys(http.lang.langs),
        }, ctx);
      },

      /**
       * ctxRender - description
       *
       * @param  {type} templateName description
       * @param  {type} ctx          description
       * @return {type}              description
       */
      ctxRender(templateName, ctx={}){
        return this.render(
          getTemplateName(templateName),
          this.getContext(ctx)
        );
      },

      contentRender(templateName, url, ctx={}){
        http.getContent(url, this.req.query, this.req.body).then((result)=>{
          if(String(result.code)[0] == "2"){
            this.status(result.code).ctxRender(templateName, Object.assign(
              {...result.data, type:result.data.result.type},
              ctx
            ));
          } else {
            const err = new Error(result.data.description);
            err.name = result.data.error;
            err.status = result.code;
            this.next(err);
          }
        });
      }
    });


    // Write original url to response header
    const originalUrl = req.originalUrl.split("#")[0].split("?")[0];
    req.res.set("X-Response-URL", originalUrl);

    // Unlocalize URL
    const relativeUrl = req.url.split("#")[0].split("?")[0];
    req.newUrl = [];
    for(const comp of relativeUrl.split("/").filter(v=>v)){
      req.newUrl.push(http.reverseTranslate(comp));
    }
    req.url = "/"+req.newUrl.join("/");

    // Save url breadcrumbs
    req.urlComps = req.newUrl.map((item, i, a)=>{
      const nameTrans = http.translate(item, req.locale);
      const link = "/"+a.slice(1, i+1).join("/");
      return {
        link: http.getLink(link, true),
        linkCanonical: http.getLink(link, false),
        name: nameTrans[0].toUpperCase()+nameTrans.slice(1),
        origName: item,
      };
    }).slice(1);


    //
    // Protecc
    //

    // DB is not ready yet
    if(!http.purky) {
      return next(new _E.CustomError("NotReady"));
    }

    // Protecc admin mode, else pass
    if(global.isAdmin){
      // If has token
      if(req.cookies.token){
        // If the token is valid
        req.user = http.purky.checkToken(req.cookies.token);
        if(req.user){
          // continue
          return next();
        // else
        }else{
          // remove token
          res.clearCookie('token');
        }
      }
      // no token, no login, no access
      return next(new _E.CustomError("NoToken"));
    }
    else{
      return next();
    }
  }
};

/**
 * getTemplateName - description
 *
 * @param  {type} name description
 * @return {type}      description
 */
function getTemplateName(name) {
  return (CONFIG.templates[name] || name)+"."+CONFIG.templates._ext;
}

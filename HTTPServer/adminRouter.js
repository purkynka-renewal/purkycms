const {Router}  = require('express');
const os = require("os");
const zhIPC = require("../ZhIPC");

module.exports = function(http){
  const router = new Router();

  router.get("/create-first-acc", (_, res)=>{
    if (!http.purky.acc_placeholder) return res.redirect(http.canonical);
    res.ctxRender("firstAccount");
  });

  router.get("/login", (_,res)=>res.redirect(http.canonical));
  router.get("/logout", (req,res)=>{
    http.purky.resolveURI(
      "/accounts/logout", {token:req.cookies.token, ...req.query}, req.body
    ).then(()=>{
      res.redirect(http.getLink("/login"));
    });
  });

  router.get("/", (_,res)=>{
    res.ctxRender("index");
  });

  router.get("/appShell", (_,res)=>{
    res.ctxRender("shell");
  });

  //
  // CONTENT MANAGEMENT
  //
  router.get("/content", (_,res)=>{
    res.contentRender("contentList", "/content/_index");
  });

  router.get("/content/:type", (req,res)=>{
    req.query.limit = Number.MAX_SAFE_INTEGER;
    res.contentRender("contentListing", "/content/"+req.params.type);
  });

  router.get("/content/:type/_new", (req,res)=>{
    res.contentRender("contentEditor", "/content/"+req.params.type);
  });

  router.get("/content/:type/:slug", (req,res)=>{
    res.contentRender("contentEditor",
        "/content/"+req.params.type+"/"+req.params.slug);
  });

  //
  // FILESYSTEM
  //
  router.get("/files*", (req,res)=>{
    const target = "/"+req.urlComps.slice(1).map(v=>v.origName).join("/");

    if (http.filesystem.exists(target)) {
      res.ctxRender("files", {
        folder: http.filesystem.listFiles(target), path: target,
      });
    } else {
      res.status(404).ctxRender("files", {folder: []});
    }
  });

  // Socket API
  http.io.of(http.getLink(CONFIG.urls.api._base+CONFIG.urls.api.files))
      .use(http.ioMiddleware())
      .use(http.ioSecurityMiddleware)
      .on('connect', (socket)=>http.filesystem.getRouter(socket, http));

  //
  // SYSTEM
  //
  router.get("/system", (_,res)=>{
    res.ctxRender("system", {
      systemAPI: http.getLink(CONFIG.urls.api._base+CONFIG.urls.api.monitoring),
      platform: process.platform, arch: process.arch, version: process.version,
      lts: !!process.release.lts, cwd: process.cwd(), kernel: os.release(),
      osuser: os.userInfo().username, hostname: os.hostname(), os: os.version(),
    });
  });

  // Socket API
  http.io.of(http.getLink(CONFIG.urls.api._base+CONFIG.urls.api.monitoring))
      .use(http.ioMiddleware())
      .use(http.ioSecurityMiddleware)
      .on('connect', (socket)=>{
        let startCPUMeasure = cpuAverage();

        socket
          .on('getThreadInfo', (res)=>{
            zhIPC.request("getThreadInfo").then(res);
          })
          .on('getUsageInfo', (res)=>{
            const totalmem = os.totalmem(), freemem = os.freemem();
            const cpuMeasure = cpuAverage();
            const idleDifference = cpuMeasure.idle - startCPUMeasure.idle;
            const totalDifference = cpuMeasure.total - startCPUMeasure.total;
            startCPUMeasure = cpuMeasure;

            res({
              cpu: 100 - ~~(100 * idleDifference / totalDifference),
              mem: (totalmem - freemem) / totalmem * 100,
            });
          })
          .on('reloadConfig', (res)=>{
            zhIPC.request('reloadConfig').then(res);
          });
      });

  return router;
};


// CPU usage calculations
function cpuAverage() {
  let totalIdle = 0, totalTick = 0;
  const cpus = os.cpus();

  for (const cpu of cpus) {
    for (const type in cpu.times) {
      totalTick += cpu.times[type];
    }
    totalIdle += cpu.times.idle;
  }
  return {idle: totalIdle / cpus.length,  total: totalTick / cpus.length};
}

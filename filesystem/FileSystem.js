const fs = require("fs");
const path = require("path");
const imageThumbnail = require("image-thumbnail");
const checkDiskUsage = require('check-disk-space');
const archiver = require('archiver');
const fileTypes = require("./fileTypes.json");

module.exports = class FileSystem {
  constructor(root, thumbs){
    this.root = path.normalize(root);
    this.thumbs = path.normalize(thumbs);
  }

  getPath(target, root=this.root) {
    return path.join(root, path.normalize(target.replace(/\.\./g, ".")));
  }

  async getDiskUsage() {
    const diskUsage = await checkDiskUsage(path.resolve(this.getPath("/")));
    return (diskUsage.size - diskUsage.free) / diskUsage.size * 100;
  }

  getThumbnail(target, type){
    const thumbTypes = global.CONFIG.thumbnails;
    if (!(type in thumbTypes)) throw new Error("Undefined thumbnail type");
    const pathArr = target.split("/").filter(v=>v);
    const name = pathArr.pop()+".jpg";
    const dest = pathArr.join("/");
    const turl = type+"/"+dest+"/";
    const srcpath = this.getPath(target);
    const thmpath = this.getPath(turl, this.thumbs);
    // File already exists
    if(fs.existsSync(thmpath+name)) return turl+name;
    // Create folder
    if(!fs.existsSync(thmpath)) fs.mkdirSync(thmpath, {recursive: true});
    // Make thumbnail asynchronously
    const options = {...thumbTypes[type], jpegOptions: { force: true }};
    imageThumbnail(srcpath, options).then((data)=>{
      const tmpfile = thmpath+"."+name;
      fs.writeFileSync(tmpfile, data);
      fs.renameSync(tmpfile, thmpath+name);
    }).catch(()=>{});
    // Return the original url before finishing
    return target;
  }

  listFiles(target="/") {
    const tpath = this.getPath(target);
    return fs.readdirSync(tpath, {withFileTypes: true })
        .filter(i=>!i.name.startsWith(".")).map(file=>({type: getFileType(file),
            ...fs.statSync(path.join(tpath,file.name)), ...file,
            link: target+"/"+file.name, isFolder: file.isDirectory()}));
  }

  rename(source, target){
    return fs.renameSync(this.getPath(source), this.getPath(target));
  }

  exists(target) {
    return fs.existsSync(this.getPath(target));
  }

  remove(target)  {
    return fs.rmdirSync(this.getPath(target), {recursive: true});
  }

  mkdir(target, options={}) {
    return fs.mkdirSync(this.getPath(target), options);
  }

  streamRead(target, options={}) {
    return fs.createReadStream(this.getPath(target), options);
  }

  streamWrite(target, options={}) {
    return fs.createWriteStream(this.getPath(target), options);
  }

  download(targets, dir){
    if(targets.length == 1){
      const target = targets[0];
      const path = this.getPath(dir+target);
      if (fs.statSync(path).isDirectory()) {
        const archive = archiver("zip");
        archive.directory(path, "/");
        archive.finalize();
        return {isArchive: true, stream: archive};
      } else {
        return {isArchive: false, stream: this.streamRead(target)};
      }
    }

    const archive = archiver("zip");
    for (const target of targets) {
      const path = this.getPath(dir+target);

      if (fs.statSync(path).isDirectory()) {
        archive.directory(path, target);
      } else {
        archive.file(path, {name: target});
      }
    }
    archive.finalize();
    return {isArchive: true, stream: archive};
  }
};


function getFileType(dirent){
  if (dirent.isDirectory()) return "folder";
  else if (dirent.isSymbolicLink()) return "link";
  else {
    let ext = dirent.name.split(".");
    if (ext.length > 1) {
      ext = ext[ext.length - 1];
      for (const [type, exts] of Object.entries(fileTypes)) {
        if (exts.includes(ext)) return type;
      }
    }
    return "file"
  }
}

function getPathinfo(path){
  const pathArr = path.split("/").filter(v=>v);
  return {
    filename: pathArr.pop(),
    dirname: "/"+pathArr.join("/")
  };
}
module.exports.getPathinfo = getPathinfo;

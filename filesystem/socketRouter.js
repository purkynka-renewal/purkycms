const ioStream = require("socket.io-stream");
const FileSystem = require("./FileSystem");
const {getPathinfo} = FileSystem;

module.exports = class SocketFS extends FileSystem{
  constructor(...args){
    super(...args);
  }

  getRouter(socket, http){
    const locale = socket.request.locale;
    // GET diskspace available
    socket.on("getDiskUsage", (res=()=>{})=>{
      this.getDiskUsage().then((diskUsage)=>res(diskUsage));
    });

    // MKDIR
    socket.on("MKDIR", (path, res=()=>{})=>{
      if(this.exists(path)){
        return res(409, http.translate("file-already-exists", locale));
      }
      this.mkdir(path);
      res(201);
    });

    // PUT a file
    ioStream(socket).on("PUT", (stream, path, res=()=>{})=>{
      const {dirname, filename} = getPathinfo(path);
      const tmpfile = dirname+"/."+filename;
      if(this.exists(path)){
        return res(409, http.translate("file-already-exists", locale));
      }
      // Create folder structure if needed
      if (!this.exists(dirname)) this.mkdir(dirname, {recursive: true});
      stream.pipe(this.streamWrite(tmpfile));
      stream.on('end', ()=>{
        this.rename(tmpfile, path);
        res(201);
      });
    });

    // DELETE a file
    socket.on("DELETE", (path, res=()=>{})=>{
      if(!this.exists(path)){
        return res(404);
      }
      this.remove(path);
      res(200);
    });

    // GET download of a file
    ioStream(socket).on("GET", (outstream, {paths, cwd}, res=()=>{})=>{
      if(!paths.every(n=>this.exists(cwd+n))){
        return res(404);
      }

      const {isArchive, stream} = this.download(paths, cwd);
      res(200, isArchive);
      stream.pipe(outstream);
    });
  }
};

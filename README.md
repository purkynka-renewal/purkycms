# sspbrno.cz web

Main module for sspbrno.cz website.

**TODO:** Write a README

- - -

## Configuration
+ `config.json` &ndash; Server-related "permanent" configuration


## Concept
### Content types
#### ~~Post~~ Article
News article ~~or a static page~~  
Fields:

+ Title
+ ~~Type (article/page)~~
+ Category
+ Content
+ Image gallery

#### Page
A long-term relevant static page   
Fields:

+ Title
+ Category
+ Content
+ Image gallery

#### Widgets
// TODO

#### Contacts
Contact information about the school and it's employees  
Fields:

+ Name
+ Phone
+ E-mail (might be generated from name for most of employees)
+ Name abbreviation

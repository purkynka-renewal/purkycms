const cluster = require("cluster");
const EventEmitter = require('events');


class ZhIPC extends EventEmitter {
  constructor(){
    super();
    this.waiters = {};

    // Master thread logic
    if (cluster.isMaster) {
      cluster.on("message", (worker, data)=>{
        this._handleMessage(data, worker);
      });
    // Worker logic
    } else {
      cluster.worker.on("message", (data)=>{
        this._handleMessage(data);
      });
    }
  }


  /**
   * _handleMessage - thread message handler
   * @private
   */
  _handleMessage({type, eventType, msgID, data}, target){
    target = target || process;

    switch (type) {
      // Request
      case 0:
        // Emit the message event with callback
        this.emit(eventType, ...data.filter(v=>v), (...data)=>{
          // Send back answer
          target.send({type: 1, eventType, msgID, data});
        });
        break;
      // Response
      case 1:
        if (!(msgID in this.waiters)) return; // Invalid message :shrug:
        // Call the callback
        this.waiters[msgID](...data);
        // Remove the no-longer-waiting waiter
        delete this.waiters[msgID];
        break;
    }
  }

  /**
   * request - requests data from master thread
   */
  request(eventType, ...data){
    const msgID = Math.random().toString(32).substr(2);

    process.send({type: 0, eventType, msgID, data});

    return new Promise((resolve)=>this.waiters[msgID] = resolve);
  }

  /**
   * send - send data to a worker
   */
  send(eventType, worker, ...data){
    const msgID = Math.random().toString(32).substr(2);

    worker.send({type: 0, eventType, msgID, data});

    return new Promise((resolve)=>this.waiters[msgID] = resolve);
  }

  /**
   * broadcast - broadcast data to all workers
   */
  broadcast(eventType, ...data){
    const promises = [];
    for (const worker of Object.values(cluster.workers)) {
      promises.push(this.send(eventType, worker, ...data));
    }
    return Promise.all(promises);
  }
}

module.exports = new ZhIPC();

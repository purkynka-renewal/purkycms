#!/usr/bin/env node
const cluster = require("cluster");

// Setup config and globals
const isProduction = global.isProduction = process.env.NODE_ENV == 'production';
const isAdmin = global.isAdmin = process.env.PURKY_MODE == 'admin';
const isWorker = cluster.isWorker;

const CONFIG = require("./config.json");
global.CONFIG = CONFIG;
CONFIG.paths.root = isAdmin ? CONFIG.paths.aRoot : CONFIG.paths.sRoot;

process.title = __filename;

//
// Libs
//
const debug = require("debug")("main");
require("colors");

//
// Init
//
if (isWorker) debug.namespace += ":"+cluster.worker.id;
else debug.enabled = true;
debug(
  "Starting in "+
  (isProduction ? "production/performance" : "developer").cyan+
  " mode as a "+
  (isWorker ? (isAdmin? "admin" : "service").yellow : "master".red)+
  " thread"
);

process.on("beforeExit", ()=>{
  debug("Exitting...");
});

//
// Start
//
if (isWorker) {
  require("./threadWorker");
} else {
  require("./threadMaster");
}

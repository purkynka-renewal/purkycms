# Original concept

+ Will run behind NGINX
+ Nginx will cache and recieve Last-Modified header
+ Nginx will also provide static files when the app is updating etc.
+ The app will cache templates for faster re-renders
+ Will render data from `purky` using `sspbrno-frontend` templates
+ Will use router from `purky-panel` which will render admin web-interface using `purky-frontend`

### Features
+ ~~pre-render views on DB update~~
+ self-updates (automated)
+ self-check functions
+ "Backend is loading" page on (re)start

### Modules
+ `purky` (CMS backend, user management)
+ `purky-panel` (admin panel web-interface)
  + `purky-frontend` (admin panel templates)
+ `sspbrno-frontend` (website templates)

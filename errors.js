module.exports = {
  CustomError: class CustomError extends Error{
    constructor(name, msg){
      super(msg);
      this.name = name;
    }
  }
}

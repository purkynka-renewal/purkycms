const CONFIG      = global.CONFIG;
const YAML        = require('yaml');
const fs          = require('fs');
const debug       = require("debug")("lang");

module.exports = (function lang() {
  return ({
    langs: {},
    onUpdate() {},
    update() {
      debug("Started loading...");
      const promises = [];
      const langsDirPath = CONFIG.paths.config._base+CONFIG.paths.config.lang;
      const langsDir     = fs.readdirSync(langsDirPath, {withFileTypes: true});

      for (const langDirent of langsDir){
        if(!langDirent.isDirectory()) continue;

        debug("Found language '"+langDirent.name+"'");
        if(!(langDirent.name in this.langs)) this.langs[langDirent.name] = {};
        const langDirPath = langsDirPath+"/"+langDirent.name;
        const langDir     = fs.readdirSync(langDirPath, {withFileTypes: true});

        for(const fileent of langDir){
          if(
            !fileent.isFile() ||
            !fileent.name.endsWith(".yml") ||
            fileent.name.startsWith("_")
          ) continue;

          const filePath = langDirPath+"/"+fileent.name;
          promises.push(new Promise((resolve)=>{
            fs.readFile(filePath, (err, file)=>{
              if(err)   return debug(err);
              if(!file) return;

              const lang          = YAML.parse(file.toString());
              const langProcessed = {};
              const process = ((obj,namespace="")=>{
                for(const key in obj){
                  const item = obj[key];
                  if(typeof item == "object") {
                    process(item, key+".");
                  }else {
                    langProcessed[namespace+key] = item;
                  }
                }
              });
              process(lang);

              Object.assign(this.langs[langDirent.name], langProcessed);

              debug(
                "Finished loading file '"+
                fileent.name+
                "' for language '"+
                langDirent.name+"'"
              );
              resolve();
            });
          }));
        }
      }

      Promise.all(promises).then(this.onUpdate);

      return this;
    },
  }).update();
})();
